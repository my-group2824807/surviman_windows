using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMove : MonoBehaviour
{

    [SerializeField] private Transform _target;
    public Transform player;
    public float rotationSpeed = 90f;

    private void Start()
    {
        transform.parent = null;
    }

    void LateUpdate()
    {
        if (_target)
        {
            transform.position = _target.position;
        }
    }


  /*  public void Povorot()
    {

        transform.RotateAround(_target.position, Vector3.up, rotationSpeed * Time.deltaTime);
    
    }*/
}
