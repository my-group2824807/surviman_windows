using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    [SerializeField] public float _speed;
    private int _numberOfCoinsInLevel;
    private float _oldMousePositionX;
    private float _eulerY;

    // private float timePressed = 0f; // ������ ������ ������
    float timer;


    [SerializeField] private Animator _animatorTimmy;

    Vector2 firstPressPos;
    Vector2 secondPressPos;
    Vector2 currentSwipe;

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            _numberOfCoinsInLevel = FindObjectOfType<CoinManager>()._numberOfCoinsInLevel;
            if (_numberOfCoinsInLevel > 10)
            {
                _numberOfCoinsInLevel = _numberOfCoinsInLevel - 10;
                FindObjectOfType<CoinManager>()._numberOfCoinsInLevel = _numberOfCoinsInLevel;
                FindObjectOfType<CoinManager>()._text.text = _numberOfCoinsInLevel.ToString();
                Time.timeScale = 0.2f;
            }
        }
        if (Input.GetMouseButtonUp(1))
        {
            Time.timeScale = 1f;
        }


        if (Input.GetKeyDown(KeyCode.Escape)) ///�����
        {
            if (Time.timeScale == 1)
            {
                Time.timeScale = 0;
            }
            else
            {
                Time.timeScale = 1;
            }
        }

        if (Input.GetMouseButtonDown(0))  // ������������ ��� ������ windows
        {
            _oldMousePositionX = Input.mousePosition.x;
            _animatorTimmy.SetBool("Run", true);
        }

        if (Input.GetMouseButton(0))
        {
            Vector3 newPosition = transform.position + transform.forward * Time.deltaTime * _speed;
            newPosition.x = Mathf.Clamp(newPosition.x, -2.5f, 2.5f);
            transform.position = newPosition;

            float deltaX = Input.mousePosition.x - _oldMousePositionX;
            _oldMousePositionX = Input.mousePosition.x;

            _eulerY += deltaX;
            _eulerY = Mathf.Clamp(_eulerY, -70f, 70f);
            transform.eulerAngles = new Vector3(0, _eulerY, 0);
        }

        if (Input.GetMouseButtonUp(0))
        {
            _animatorTimmy.SetBool("Run", false);
        }


        //������� ������ ������ ������     ��� (�����)

        if (Input.GetKey(KeyCode.Space))
        {
            _numberOfCoinsInLevel = FindObjectOfType<CoinManager>()._numberOfCoinsInLevel;
           
            if (_numberOfCoinsInLevel>0) 
            {
                _speed = 20;          
                timer += Time.deltaTime;
                if (timer >= 0.2f)
                          {
                                timer = 0f;
                                _numberOfCoinsInLevel -= 1;
                                
                                FindObjectOfType<CoinManager>()._numberOfCoinsInLevel = _numberOfCoinsInLevel;
                                FindObjectOfType<CoinManager>()._text.text = _numberOfCoinsInLevel.ToString();
                           }
            }
            else _speed = 10;
        }

        if (Input.GetKeyUp(KeyCode.Space)) //������ ���������
        {
            _speed = 10;
        }
    }


    public void Down()
    {
        _animatorTimmy.SetTrigger("Down");

    }
}
