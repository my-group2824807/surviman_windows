using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Coin : MonoBehaviour
{

    [SerializeField] private float _rotationSpeed;
  //  [SerializeField]  AudioSource _audioSource;



    void Update()
    {
        transform.Rotate(0, 360 * Time.deltaTime, 0);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
           // if (_audioSource != null)
           // {
           ///     _audioSource.Play();
           // }
            FindObjectOfType<CoinManager>().AddOne();
          //  GetComponent<AudioSource>().Play();
            Destroy(gameObject);
        }
    }

}
