using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EagleMove : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private float _speedEagle;

    public float leftLimit = -5f;
    public float rightLimit = 5f;
   
    //  [SerializeField] private Animator _animatorEagle;
    void Start()
    {
        
    }

    void Update()
    {
               

        if (transform.position.x >= rightLimit)
        {  
            transform.Rotate(0f, 180f, 0f);
        }
        
        if (transform.position.x <= leftLimit)
        { 
            transform.Rotate(0f, 180f, 0f);

        }
        transform.Translate(0, 0, _speedEagle * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GetComponent<AudioSource>().Play();
            FindObjectOfType<PlayerMove>().Down();
            FindObjectOfType<GameManager>().ShowLoseWindow();
        }
    }
        //     if (transform.position.x <= leftLimit || transform.position.x >= rightLimit)
        //      {
        //          speed *= -1;
        //          transform.Rotate(0f, 270f, 0f);
        //      }
        //      transform.Translate(0f, 0f, speed * Time.deltaTime * _speedEagle);



        //  gameObject.SetActive(false);
        // }





    }
