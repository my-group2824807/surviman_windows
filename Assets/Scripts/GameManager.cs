using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [DllImport("__Internal")]
    private static extern void ShowAdv();
    [SerializeField] GameObject _startMenu;
    [SerializeField] TextMeshProUGUI _levelText;
    [SerializeField] TextMeshProUGUI _SpeedText;
    [SerializeField] TextMeshProUGUI _FPSText;
    [SerializeField] GameObject _finishWindow;
    [SerializeField] GameObject _LoseWindow;
     
    private bool isPaused = false;
    


    IEnumerator FPS()
    {
        while (true)
        {
            float fps = 1f / Time.deltaTime;
            _FPSText.text = "���: " + Mathf.RoundToInt(fps).ToString();
            yield return new WaitForSeconds(1f);
        }
    }


    private void Start()
    {

       // ShowAdv(); /// ���������� ������������� ������� ������ ������� ����� ������ SDK
         StartCoroutine(FPS());
         _levelText.text = "�������: " + (SceneManager.GetActiveScene().buildIndex + 1);
    }

    void Update()
    {
        GameObject obj = GameObject.Find("Player");
        _SpeedText.text = "��������: " + obj.GetComponent<PlayerMove>()._speed.ToString();
    }

            public void Play()
    {
        _startMenu.SetActive(false);
        FindObjectOfType<PlayerBehaviour>().Play();
        FindObjectOfType<FatManMagn>().Run();
        Time.timeScale = 1;
    }
    public void ShowFinishWindow()
    {
        _finishWindow.SetActive(true);
        Time.timeScale = 0;
    }
    public void ReLoad()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void ShowLoseWindow()
    {
        _LoseWindow.SetActive(true);      
        Time.timeScale = 0;
    }

    public void NextLevel()
    {
        int next = SceneManager.GetActiveScene().buildIndex + 1;
        if (next < SceneManager.sceneCountInBuildSettings)
        {
            SceneManager.LoadScene(next);
        }


    }

    public void Pause()
    {
        if (isPaused)
        {
            Time.timeScale = 1;
            isPaused = false;
        }
        else
        {
            Time.timeScale = 0;
            isPaused = true;
        }


    }
}
